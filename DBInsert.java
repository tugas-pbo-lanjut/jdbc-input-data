package com.d3ti.pbolanjut20.jbdc;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.GridLayout;
import javax.swing.JLabel;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;
import com.jgoodies.forms.layout.FormSpecs;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JTextPane;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class DBInsert extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DBInsert frame = new DBInsert();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public DBInsert() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new FormLayout(new ColumnSpec[] {
				FormSpecs.RELATED_GAP_COLSPEC,
				FormSpecs.DEFAULT_COLSPEC,
				FormSpecs.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("default:grow"),},
			new RowSpec[] {
				FormSpecs.RELATED_GAP_ROWSPEC,
				FormSpecs.DEFAULT_ROWSPEC,
				FormSpecs.RELATED_GAP_ROWSPEC,
				FormSpecs.DEFAULT_ROWSPEC,
				FormSpecs.RELATED_GAP_ROWSPEC,
				FormSpecs.DEFAULT_ROWSPEC,
				FormSpecs.RELATED_GAP_ROWSPEC,
				FormSpecs.DEFAULT_ROWSPEC,
				FormSpecs.RELATED_GAP_ROWSPEC,
				FormSpecs.DEFAULT_ROWSPEC,
				FormSpecs.RELATED_GAP_ROWSPEC,
				RowSpec.decode("default:grow"),
				FormSpecs.RELATED_GAP_ROWSPEC,
				FormSpecs.DEFAULT_ROWSPEC,}));
		
		JLabel lblNewLabel = new JLabel("NIM");
		contentPane.add(lblNewLabel, "2, 2, left, default");
		
		textField = new JTextField();
		contentPane.add(textField, "4, 2, left, default");
		textField.setColumns(10);
		
		JLabel lblNewLabel_1 = new JLabel("Nama");
		contentPane.add(lblNewLabel_1, "2, 4, left, default");
		
		textField_1 = new JTextField();
		contentPane.add(textField_1, "4, 4, left, default");
		textField_1.setColumns(10);
		
		JLabel lblNewLabel_2 = new JLabel("Jenis Kelamin");
		contentPane.add(lblNewLabel_2, "2, 6, left, default");
		
		JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"laki-laki", "perempuan"}));
		contentPane.add(comboBox, "4, 6, left, default");
		
		JLabel lblNewLabel_3 = new JLabel("Tempat Lahir");
		contentPane.add(lblNewLabel_3, "2, 8, left, default");
		
		textField_2 = new JTextField();
		contentPane.add(textField_2, "4, 8, left, default");
		textField_2.setColumns(10);
		
		JLabel lblNewLabel_4 = new JLabel("Tanggal Lahir");
		contentPane.add(lblNewLabel_4, "2, 10, left, default");
		
		textField_3 = new JTextField();
		contentPane.add(textField_3, "4, 10, left, default");
		textField_3.setColumns(10);
		
		JLabel lblNewLabel_5 = new JLabel("Alamat");
		contentPane.add(lblNewLabel_5, "2, 12, left, top");
		
		JTextPane textPane = new JTextPane();
		contentPane.add(textPane, "4, 12, default, fill");
		
		JButton btnNewButton = new JButton("Simpan");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String nim = textField.getText();
				String nama = textField_1.getText();
				String jk = comboBox.getSelectedItem().toString();
				String tmp_lahir = textField_2.getText();
				String tgl_lahir = textField_3.getText();
				String alamat = textPane.getText();
				DBBiodata biodata = new DBBiodata();
				biodata.insertBiodata(nim, nama, jk, tmp_lahir, tgl_lahir, alamat);
			}
		});
		contentPane.add(btnNewButton, "4, 14, left, center");
	}

}
